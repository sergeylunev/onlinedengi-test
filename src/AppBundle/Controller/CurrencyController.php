<?php

namespace AppBundle\Controller;

use GuzzleHttp\Client;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CurrencyController extends Controller
{
    /**
     * @Route("/get_currency", name="get_currency")
     */
    public function getAction()
    {
        $content = $this->get('app.request_client_service')->getCurrencyList();
        $result = $this->get('app.currency_xmlhandler')->handleCurrencyXML($content);

        return $this->render('AppBundle:currency:get.html.twig', [
            'currency' => $result
        ]);
    }
}
