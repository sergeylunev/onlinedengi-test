<?php

namespace AppBundle\Service;

class CurrencyCalculator
{
    /**
     * Calculate currency based on RUB
     *
     * @param float $value
     * @return float
     */
    public function calculate($value)
    {
        return 1 / ($value / 1);
    }
}