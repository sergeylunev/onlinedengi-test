<?php

namespace AppBundle\Service;

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Request;

class RequestClientService
{
    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @return string
     */
    public function getCurrencyList()
    {
        $result = $this->client->request(
            Request::METHOD_POST,
            'https://www.onlinedengi.ru/dev/xmltalk.php',
            [
                'form_params' => [
                    'xml' => '<request><action>get_currency_rates</action></request>'
                ]
            ]
        );

        return $result->getBody()->getContents();
    }
}