<?php

namespace AppBundle\Service;

use AppBundle\Currency\DTO\CurrencyList;
use AppBundle\Currency\DTO\CurrencyValue;

class CurrencyXMLHandler
{
    /**
     * @var CurrencyCalculator
     */
    private $calculator;

    /**
     * @param CurrencyCalculator $calculator
     */
    function __construct(CurrencyCalculator $calculator)
    {
        $this->calculator = $calculator;
    }


    /**
     * @param string $xml
     * @return CurrencyList
     */
    public function handleCurrencyXML($xml)
    {
        $xml = new \SimpleXMLElement($xml);
        $currencyList = new CurrencyList();

        foreach ($xml->currency as $currency) {
            $value = $this->calculator->calculate((float)$currency->value);
            $currencyValue = new CurrencyValue((string)$currency->name, $value);
            $currencyList->add($currencyValue);
        }

        return $currencyList;
    }
}