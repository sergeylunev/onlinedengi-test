<?php

namespace AppBundle\Currency\DTO;

class CurrencyList
{
    public $list = [];

    /**
     * @param CurrencyValue $value
     */
    public function add(CurrencyValue $value)
    {
        $this->list[] = $value;
    }
}