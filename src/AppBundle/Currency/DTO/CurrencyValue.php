<?php

namespace AppBundle\Currency\DTO;

class CurrencyValue
{
    const USD = 'USD';
    const RUB = 'RUB';
    const EUR = 'EUR';
    const UAH = 'UAH';
    const BYR = 'BYR';
    const UZS = 'UZS';

    protected $currencyNames = [
        self::USD => 'US Dollar',
        self::RUB => 'Russian Ruble',
        self::EUR => 'Euro',
        self::UAH => 'Hryvnia',
        self::BYR => 'Belarussian Ruble',
        self::UZS => 'Uzbekistan Sum'
    ];

    /**
     * @var string
     */
    public $name;
    /**
     * @var float
     */
    public $value;

    /**
     * @param string $name
     * @param float  $value
     */
    public function __construct($name, $value)
    {
        $this->name = $this->currencyNames[$name];
        $this->value = $value;
    }
}